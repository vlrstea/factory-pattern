<?php

interface Shape{

	public function draw();
}

class Rectangle implements Shape
{	
	private $postion;
	public $x;
	public $y;
	public $w;
	public $h;

	public function __construct( $pos )
	{

		$this->position = $pos;
	}
	public function draw()
	{

		echo 'Drawing a rectangle';
	}
}
class Position{}

class ShapeFactory
{

	public function create( $type )
	{

		if( $type == 'Rectangle' )
		{

			return new Rectangle( new Position );
		}
	}

}

$factory = new ShapeFactory();
$rect = $factory->create('Rectangle');
$rect->draw();