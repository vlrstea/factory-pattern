<?php
//this class provides the right factory
class AdapterFactory
{

	public function make( $config )
	{

		if( $config instanceof Config)
		{

			switch($config->get('text.default'))
			{

				case'json':
					return new JSONAdapter;
				break;

				case'xml':
					return new XMLAdapter;
				break;

				case'csv':
					return new CSVAdapter;
				break;
				
			}
		} else {

			switch($config)
			{

				case TextEnum::JSON:
					return new JSONAdapter;
				break;

				case TextEnum::XML:
					return new XMLAdapter;
				break;

				case TextEnum::CSV:
					return new CSVAdapter;
				break;
			}
		}

	}
}