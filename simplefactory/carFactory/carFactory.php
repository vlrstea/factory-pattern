<?php

class CarFactory
{


	public function create( $brand )
	{

		switch($brand)
		{

			case'jeep':
				$this->car = new Jeep;
			break;

			case'dacia':
				$this->car = new Dacia;
			break;

			case'ford':

				$this->car = new Ford;
			break;

			default:
				echo 'No car of this type exists';
			break;
		}

	}
}