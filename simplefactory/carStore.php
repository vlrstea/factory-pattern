<?php

class carStore

{
	protected $carFactory;

	public function __construct()
	{

		$this->carFactory = new carFactory();
	}

	public function buyCar( $brand )
	{

		$this->carFactory->create($brand);
	}

}